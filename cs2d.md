#  🎮 Installing CS2D on Linux.
## Introduction
CS2D is a top-down multiplayer shooter resembling Counter-Strike.

🚀 This guide simplifies the installation process, making CS2D setup a breeze!

### ``[Arch-Linux is not supported]``

## Installation Steps
### Step 1: Download CS2D and Unzip It.

1. Download client from [CS2D website](https://cs2d.com/download) 
2. Make ~/opt folder and Move to the ~/opt folder
```sh
mkdir ~/opt && mv [NAME_OF_DOWNLOADED_FILE] ~/opt 
```
3. Open Terminal and Unzip the file
```sh
unzip [NAME_OF_DOWNLOADED_FILE] -d CS2D && cd CS2D
```
### Step 2: Installation of Required Packages to Run CS2D
1. Add x86 Architecture Packages 
```sh
sudo iptables -A INPUT -p udp -m length --length 0:28 -j DROP && sudo dpkg --add-architecture i386
```
2. Installation of x86 Packages.
```sh
sudo apt-get update && sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386
```
3. [Optional] If sound of CS2D doesn't work
```sh
sudo apt-get install libopenal-dev:i386 
```
### Step 3. Running CS2D and adding it Desktop Shortcut
1. Make CS2D executable
```sh
chmod +xx ./CS2D 
```
2. Copy Steam Library to /usr/libc
```sh
sudo cp ./libsteam_api.so /usr/lib/
```
3. Now Run CS2D and Check if it giving any error
```sh
./CS2D
```
If it give error segmentation fault then 
```sh
sudo ./CS2D
```
4. Now Let's create a Desktop Shortcut
```sh
cd /usr/local/share/applications && sudo nano CS2D.desktop
```
Add this to CS2D.desktop
```
[Desktop Entry]
Type=Application
Name=CS2D
Comment=CS2D. A top-down 2D shooter game.
Exec=~/opt/CS2D/CS2D
Icon=~/opt/CS2D/gfx/cs2d.bmp
Terminal=false
```
5. Logout and Log back in..

#### ⚡️ Voilà! CS2D got installed on your Linux machine. Now, Enjoy the game!

[Coderberg](https://codeberg.org/sag/guide/src/branch/main/cs2d.md) | [Bundles](https://bundles.cc/How-to-install-CS2D-on-Linux)