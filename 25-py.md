## 1.  Program to sort 3 numbers in descending order using if-else statements 
``` python
a = int(input("Enter first number: "))
b = int(input("Enter second number: "))
c = int(input("Enter third number: "))
if a > b:
    if b > c:
        print("Numbers in sorted order:", a, " ", b, " ", c)
    else:
        if c > a:
            print("Numbers in sorted order:", c, " ", a, " ", b)
        else:
            print("Numbers in sorted order:", a, " ", c, " ", b)
elif b > a:
    if a > c:
        print("Numbers in sorted order:", b, " ", a, " ", c)
    else:
        print("Numbers in sorted order:", b, " ", c, " ", a)
else:
    print("Numbers in sorted order:", c, " ", b, " ", a)
```


## 2.  Program to find the Armstrong number between two numbers 
``` python
lwr = int(input("Lower Number: "))
uppr = int(input("Upper Number: "))
for numb in range(lwr, uppr + 1):
   odr = len(str(numb))
   sum = 0
   tem = numb
   while tem > 0:
       digit = tem % 10
       sum += digit ** odr
       tem //= 10
   if numb == sum:
       print("The Armstrong numbers are: ",numb)
```


## 3.  Program to find factorial of a number.
``` python
import math
n = int (input ("Enter the number whose factorial you want to find: "))
print ("The factorial of the number is: ")
print (math.factorial (n))            
```

## 4.  Program to largest number from 3 numbers.
``` python
a = int(input("Enter first number: "))
b = int(input("Enter second number: "))
c = int(input("Enter third number: "))                  
lis = [a, b, c]
print("Largest number in the list is:", max (lis))            
```


## 5.  Program to find a number is odd or even 
``` python
num = int (input ("Enter any number to test whether it is odd or even: "))
if (num % 2) == 0:
              print ("The number is even")
else:
              print ("The provided number is odd")           
```


## 6.  Program to find sum of first n natural number
``` python
n = int(input("Enter any natural number: "))
if n < 0:
              print ("Wrong input. Please enter a positive number.")
else:
              sum = 0
              while (n > 0):
                             sum +=n
                             n -=1
              print ("The sum of the natural numbers is:", sum)         
```


## 7.  Program to find the ASCII value of a character
``` python
c = input("Enter the number whose ASCII code needs to be found: ")
print ("The ASCII value of given character is", ord (c))          
```


## 8.  Program to convert Decimal Number into Binary
``` python
def convertToBin(a):
   if a > 1:
       convertToBin(a//2)
   print(a % 2,end = '')
d = int(input("Enter a Decimal Number: "))
convertToBin(d)
print()
            
```


## 9.  Program to find the factor of a number
``` python
numb=int(input("Enter any number: "))
facts=[]
for a in range(1,numb+1):
    if numb%a==0:
       facts.append(a)
print ("Factors of {} = {}".format(numb,facts))           
```


## 10.  Program to find LCM of two numbers
``` python
def calc_gcd (x, y):
              while (y):
                            x, y = y, x % y
              return x
def calc_lcm (x, y):
              lcm = (x*y) // calc_gcd (x, y)
              return lcm
num1 = int(input("Enter the first number: "))
num2 = int(input ("Enter the second number: "))
print ("The LCM of the given numbers is", calc_lcm(num1, num2))            
```


## 11.  Program to find the Square root of a given number
``` python
numb = float(input('Enter a number: '))
numsqrt = numb ** 0.5
print('Square root of %0.3f is %0.3f'%(numb ,numsqrt))         
```

## 12.  Program to print the table of a number
``` python
numb = int(input("Enter a number : "))    
print("Table of: ")  
for a in range(1,11):    
   print(numb,'x',a,'=',numb*a)     
```


## 13.  Program to find the HCF of two numbers
``` python
def calc_hcf(x, y):
    if x > y:
        smaller = y
    else:
        smaller = x
    for i in range(1, smaller + 1):
        if x % i == 0 and y % i == 0:
            hcf = i
    return hcf
num1 = int(input("Enter the first number as required: "))
num2 = int(input("Enter the second number to find the HCF: "))
print("The HCF of the two numbers entered is", calc_hcf(num1, num2))          
```


## 14.  Program to check any year is Leap year or not
``` python
year = int(input("Enter any year that is to be checked for a leap year: "))
if (year % 4) == 0:
    if (year % 100) == 0:
        if (year % 400) == 0:
            print("The given year is a leap year")
        else:
            print("It is not a leap year")
    else:
        print("The given year is a leap year")
else:
    print("It is not a leap year")          
```

## 15.  Program to find area of Triangle
``` python
p = float(input('Enter the length of the first side: '))
q = float(input('Enter the length of the second side: '))
r = float(input('Enter the length of the final side: '))
s = (p + q + r) / 2
area_tri = (s * (s - p) * (s - q) * (s - r)) ** 0.5
print('The area of the triangle is %0.2f' % area_tri)           
```


## 16.  Program to convert Fahrenheit Temperature to Celsius and Kelvin
``` python
fahren = float(input("Enter Temperature in Fahrenheit: "))
celcius = (fahren - 32) / 1.8
kelvin = celcius + 273
print("Equivalent Temperature in Celsius:", celcius)
print("Equivalent Temperature in Kelvin:", kelvin)         
```

## 17.  Program to convert KM to Miles and Meter
``` python
ikm = float(input("Enter a number to convert: "))
conversion_factor = 0.621371
meter = km * 1000.0
miles = km * conversion_factor
print('%0.2f km is equal to %0.2f miles' %(km,miles))
print('%0.2f km is equal to %0.2f meters' %(km,meter))            
```


## 18.  Program to get random number between 1 and 10000
``` python
import random
randomlist = []
for a in range(0, 4):
    q = random.randint(1, 10000)
    randomlist.append(q)
print(randomlist)          
```


## 19.  Program to get random Head or Tail
``` python
import random
def coin_toss():
    result = random.choice(["Heads", "Tails"])
    return result
toss_result = coin_toss()
print("Coin Toss Result: " + toss_result)        
```


## 20.  Program to sort words in alphabetical order of a sentence
``` python
str_1 = input("Enter a string: ")
words = [word.lower() for word in str_1.split()]
words.sort()
print("The words sorted in alphabetical order are as follows: ")
for word in words:
    print(word)          
```


## 21.  Program to find a Triangle is equilateral or not.
``` python
def is_equilateral_triangle(side1, side2, side3):
    if side1 == side2 == side3:
        return True
    else:
        return False
side1 = float(input("Enter the length of the first side: "))
side2 = float(input("Enter the length of the second side: "))
side3 = float(input("Enter the length of the third side: "))
if is_equilateral_triangle(side1, side2, side3):
    print("It is an equilateral triangle.")
else:
    print("It is not an equilateral triangle.")       
```

## 22.  Program to find the area of a Circle
``` python
import math
radius = float(input("Enter the radius of the circle: "))
area = math.pi * radius ** 2
print("The area of the circle is:", area)          
```


## 23.  Program to find a word is Palindrome or not.
``` python
word = input("Enter a word: ")
if word == word[::-1]:
    print("It's a palindrome!")
else:
    print("It's not a palindrome.")            
```


## 24.  Program to find Simple Interest.
``` python
principal = float(input("Enter principal amount: "))
rate = float(input("Enter rate of interest: "))
time = float(input("Enter time (in years): "))
simple_interest = (principal * rate * time) / 100
print("Simple interest:", simple_interest)         
```


## 25.  Program to count number of words in a sentence
``` python
sentence = input("Enter a sentence: ")
word_count = len(sentence.split())
print("Number of words in the sentence:", word_count)    
```
